package com.example.hello

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.hello.R.layout


class MainActivity : AppCompatActivity() {
    private val TAG = "HELLO:"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layout.activity_main)
    }

    fun showMessage(view: View?) {
        val name: TextView = findViewById(R.id.textViewName)
        val stuId: TextView = findViewById(R.id.textViewStuID)
        Log.d(TAG, "Name: ${name.text} | Student ID: ${stuId.text}")

        val Intent = Intent(this, HelloActivity::class.java)
        Intent.putExtra("name", name.text)
        startActivity(Intent)
    }
}