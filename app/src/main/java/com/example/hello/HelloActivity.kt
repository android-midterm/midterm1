package com.example.hello

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class HelloActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.hello_activity)
        val displayName: TextView = findViewById(R.id.textViewStuName)
        val name: String= intent.getStringExtra("name").toString()
        displayName.text= name
    }
}